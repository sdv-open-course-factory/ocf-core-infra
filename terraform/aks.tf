# Kubernetes
resource "random_id" "prefix" {
  byte_length = 8
}

module "aks" {
  source  = "Azure/aks/azurerm"
  version = "7.1.0"

  prefix                    = random_id.prefix.hex
  resource_group_name       = var.resource_group_name
  kubernetes_version        = "1.24"
  automatic_channel_upgrade = "patch"
  agents_pool_name          = "ocfcoreagent"
  cluster_name              = "ocfcore"
  rbac_aad                  = false

  public_network_access_enabled   = true
  network_plugin                  = "azure"
  network_policy                  = "azure"
  os_disk_size_gb                 = 60
  sku_tier                        = "Free"
  vnet_subnet_id                  = module.network.vnet_subnets[0]
  log_analytics_workspace_enabled = true


  depends_on = [azurerm_resource_group.rg]
}