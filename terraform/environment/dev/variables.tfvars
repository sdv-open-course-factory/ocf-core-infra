env = "dev"
# RG
resource_group_name = "OCFCoreInfraDev"
resource_group_location = "westeurope"

# VPC
vpc_name = "OCFCoreInfraDevVPC"
vpc_cidr_block = ["10.30.0.0/16"]
subnet_name = "OCFCoreInfraDevSubnet"
nsg_name = "OCFCoreInfraDevNSG"
subnet_cidr_block = "10.30.1.0/24"







