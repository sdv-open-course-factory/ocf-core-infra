module "k8s" {
  source  = "./modules/kubernetes"
  img_tag = var.img_tag
  env     = var.env
}