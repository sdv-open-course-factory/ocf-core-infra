## BACKEND ##
resource "kubernetes_deployment" "ocf-core-backend" {
  metadata {
    name = "ocf-core-backend"
    labels = {
      App = "OcfCoreBackend"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        App = "OcfCoreBackend"
      }
    }
    template {
      metadata {
        labels = {
          App = "OcfCoreBackend"
        }
      }
      spec {
        container {
          image = "registry.gitlab.com/sdv-open-course-factory/ocf-core/${var.env}-backend:${var.img_tag}"
          name  = "ocf-core-backend"

          port {
            container_port = 80
          }

          port {
            container_port = 443
          }

          port {
            container_port = 8000
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}


resource "kubernetes_service" "ocf-core-backend" {
  metadata {
    name = "ocf-core-backend-service"
  }

  spec {
    selector = {
      App = "OcfCoreBackend"
    }

    type = "LoadBalancer"

    port {
      name        = "http"
      protocol    = "TCP"
      port        = 80
      target_port = 80
    }

    port {
      name        = "https"
      protocol    = "TCP"
      port        = 443
      target_port = 443
    }

    port {
      name        = "swagger"
      protocol    = "TCP"
      port        = 8000
      target_port = 8000
    }
  }
}

## POSTGRESQL ##
resource "kubernetes_deployment" "postgresql" {
  metadata {
    name = "postgresql"
    labels = {
      App = "PostgreSQL"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        App = "PostgreSQL"
      }
    }
    template {
      metadata {
        labels = {
          App = "PostgreSQL"
        }
      }
      spec {
        container {
          image = "postgres:latest"
          name  = "postgresql"

          env {
            name  = "POSTGRES_USER"
            value = "login"
          }

          env {
            name  = "POSTGRES_PASSWORD"
            value = "password"
          }

          env {
            name  = "POSTGRES_DB"
            value = "go_db"
          }

          volume_mount {
            name       = "postgresql-data"
            mount_path = "/var/lib/postgresql/data"
          }
        }

        volume {
          name = "postgresql-data"

          empty_dir {
            medium = "Memory"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "postgresql" {
  metadata {
    name = "postgresql-service"
  }

  spec {
    selector = {
      App = "PostgreSQL"
    }

    type = "ClusterIP"

    port {
      name        = "postgresql"
      protocol    = "TCP"
      port        = 5432
      target_port = 5432
    }
  }
}

/*## FRONTEND ##
resource "kubernetes_deployment" "ocf-core-frontend" {
  metadata {
    name = "ocf-core-frontend"
    labels = {
      App = "OcfCoreFrontend"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "OcfCoreFrontend"
      }
    }
    template {
      metadata {
        labels = {
          App = "OcfCoreFrontend"
        }
      }
      spec {
        container {
          image = "registry.gitlab.com/sdv-open-course-factory/ocf-core/${var.env}-frontend:${var.img_tag}"
          name  = "ocf-core-frontend"

          port {
            container_port = 80
          }
          port {
            container_port = 443
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "ocf-core-frontend" {
  metadata {
    name = "ocf-core-frontend-service"
  }

  spec {
    selector = {
      App = "OcfCoreFrontend"
    }

    type = "LoadBalancer"

    port {
      name        = "http"
      protocol    = "TCP"
      port        = 80
      target_port = 80
    }
    port {
      name        = "https"
      protocol    = "TCP"
      port        = 443
      target_port = 443
    }
  }
}*/

