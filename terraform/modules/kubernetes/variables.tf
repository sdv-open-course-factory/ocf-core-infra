variable "img_tag" {
    description = "Image tag"
    type        = string
}

variable "env" {
    description = "Environment"
    type        = string
}