output "kube-host" {
  description = "ID of the resource group"
  value       = module.aks.host
  sensitive   = true
}
