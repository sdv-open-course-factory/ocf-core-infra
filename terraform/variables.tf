# Global
variable "location" {
  description = "Azure region"
  type        = string
  default     = "westeurope"
}

variable "env" {
  description = "Environment"
  type        = string
}

# Env specific
# Apply using terraform apply -var-file=environment/dev/variables.tfvars

## Resource group
variable "resource_group_name" {
  description = "Name of the resource group"
  type        = string
}

variable "resource_group_location" {
  description = "Azure region location"
  type        = string
}

## VPC
variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
}

variable "vpc_cidr_block" {
  description = "CIDR block for the VPC"
  type        = list(string)
}

variable "subnet_name" {
  description = "Subnet name"
  type        = string
}

variable "nsg_name" {
  description = "Network security group name"
  type        = string
}

variable "subnet_cidr_block" {
  description = "CIDR block for the subnet"
  type        = string
}

variable "img_tag" {
  description = "Image tag"
  type        = string
}
