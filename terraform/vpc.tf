# VPC
module "network" {
  source              = "Azure/network/azurerm"
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = "10.30.0.0/16"
  vnet_name           = var.vpc_name
  subnet_prefixes     = ["10.30.1.0/24"]
  subnet_names        = ["aks-main"]
  use_for_each        = true
  tags = {
    environment = "dev"
  }
  depends_on = [azurerm_resource_group.rg]
}